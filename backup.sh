#!/bin/bash

BACKUPNAME=backup-vm.tar.gz

# backup
tar -cvpzf /tmp/$BACKUPNAME /etc /home /var/log/bind9 /var/log/mail.log /var/log/messages /var/log/dovecot/dovecot.log /var/www/vhosts /var/www/html /var/web /opt/roundcube

# copy to share
cp -avr /tmp/$BACKUPNAME /media/sf_share/