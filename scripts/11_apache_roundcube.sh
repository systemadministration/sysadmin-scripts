#!/bin/bash

source ./util.sh
source ./config.sh


printInfo "Apache-Roundcube setup started..."

printInfo "Removing roundcube..."
rm -vr /opt/roundcube

printInfo "Creating roundcube directory..."
mkdir -p /opt/roundcube/roundcubemail-1.1.4

printInfo "Extracting roundcube..."
tar -xzvf $SHARE_LOC/roundcubemail-1.1.4-complete.tar.gz -C /opt/roundcube/

printInfo "Creating symbolic link..."
cd /opt/roundcube
ln -s /opt/roundcube/roundcubemail-1.1.4/ roundcubemail

printInfo "Replacing domain name in config.inc.php..."
MAINCONFIG=$NEWROOT/opt/roundcube/roundcubemail-1.1.4/config/config.inc.php
var="$DOMAIN"
sed -i "s|mehmet91\.est|$var|g" "$MAINCONFIG"
var="12345"
sed -i "s|\*eo1kecD\*|$var|g" "$MAINCONFIG"

printInfo "Copying config.inc.php..."
cp -avr $NEWROOT/opt/roundcube/roundcubemail-1.1.4/config/config.inc.php /opt/roundcube/roundcubemail-1.1.4/config/config.inc.php

printInfo "Setting roundcube ownership to www-data..."
chown -R www-data /opt/roundcube/roundcubemail-1.1.4
chown www-data /opt/roundcube/roundcubemail-1.1.4/config/config.inc.php

printWarn "Create roundcube user in mysql. Refer to manual! And do not forget to restart apache."
mysql -p -u root