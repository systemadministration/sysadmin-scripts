#!/bin/bash

##############################################################
# File System
##############################################################
export NEWROOT=/mnt/NewRoot
export SHARE_LOC=/media/usb/sysadmin
export BACKUP_NAME=backup-vm.tar.gz

export ADMINUSER=admin


##############################################################
# Network
##############################################################
## VM
export HOSTNAME=examhost
export NAME=mehmet91
export TOPDMN=xyz
export DOMAIN=mehmet91.$TOPDMN
export TEACHER=teacher.$TOPDMN

export IPEND=166
export LOCALIP=192.168.15.$IPEND
export NETMASK=255.255.255.0
export IPRANGE=192.168.15.0/24
export GATEWAY=192.168.15.1

export REVERSEIP=15.168.192
export INADDRARPA=$REVERSEIP.in-addr.arpa
export ARPAZONE=$INADDRARPA.zone

export SITE_ALIAS1=alias1
export SITE_ALIAS2=alias2

## HOST
export HOST_IP_RANGE=192.168.10.0/24


##############################################################
# SSL
##############################################################
## Common
export SSL_COUNTRY="EE"
export SSL_STATE="Tartumaa"
export SSL_LOCALITY="Tartu"
export SSL_ORGANIZATION_NAME="SA Lab"

## MAIL
export MAIL_ORGANIZATION_UNIT="Mail Server"
export MAIL_COMMON_NAME="mail.$DOMAIN"

export MAILSSLDOVECOT=dovecot-exam
export MAILSSLPOSTFIX=postfix-exam
export MAIL_SSL_PEM_LOC=/etc/ssl/certs
export MAIL_SSL_KEY_LOC=/etc/ssl/private

## APACHE
export AP_COMMON_NAME="CA of SA Lab"
export AP_ORGANIZATION_UNIT="Web Masters"
export AP_ORGANIZATION_UNIT_KEY="Web Admins"
export AP_EMAIL="mailuser@$DOMAIN"

export APACHE_CERT_LOC=/etc/ssl/cacert
export APACHEROOTCA=rootCA-exam
export APACHECERTSUFFIX=exam


##############################################################
# Samba, NFS
##############################################################
export DATAGUY_USER=dataguy

export SAMBA_RW_PATH=/srv/data/rw
export SAMBA_RO_PATH=/srv/data/ro

export NFS_PATH=/srv/data
export NFS_RW_PATH=/srv/data/rw
export NFS_RO_PATH=/srv/data/ro

export NFS_RW_REMOTE=/media/nfs_rw
export NFS_RO_REMOTE=/media/nfs_ro

export MY_NFS_RW_DIR=$DOMAIN:/rw
export MY_NFS_RO_DIR=$DOMAIN:/ro
export MY_NFS_PORT=2049
export TEACHER_NFS_RW_DIR=$TEACHER:/rw
export TEACHER_NFS_RO_DIR=$TEACHER:/ro
export TEACHER_NFS_PORT=2049


##############################################################
# LDAP
##############################################################
export LDAP_CERT_PATH=/etc/ssl/certs/ldapCA.pem
export LDAP_BASE="dc=sandbox,dc="$TOPDMN
export LDAP_URI=ldaps://ldap.$TEACHER/