#!/bin/bash

source ./util.sh
source ./config.sh

printInfo "Samba setup started..."

printInfo "Creating $DATAGUY_USER user..."
adduser $DATAGUY_USER

printInfo "Installing packages..."
apt-get install samba samba-client


printInfo "Setting samba read-write path in etc/samba/smb.conf..."
var="$SAMBA_RW_PATH"
find $NEWROOT/etc/samba/smb.conf -type f -exec sed -i "s|\/srv\/data\/rw|$SAMBA_RW_PATH|g" {} \;
printInfo "Setting samba read-only path in etc/samba/smb.conf..."
var="$SAMBA_RO_PATH"
find $NEWROOT/etc/samba/smb.conf -type f -exec sed -i "s|\/srv\/data\/rw|$SAMBA_RO_PATH|g" {} \;

printInfo "Replacing etc/samba/smb.conf"
cp -avr $NEWROOT/etc/samba/smb.conf /etc/samba/smb.conf


printInfo "Creating directories for samba shares..."
mkdir -vp $SAMBA_RW_PATH
mkdir -vp $SAMBA_RO_PATH

printInfo "Setting up permissions and ownership on $SAMBA_RW_PATH, $SAMBA_RO_PATH for user $DATAGUY_USER"
chown -R $DATAGUY_USER:users $SAMBA_RW_PATH
chown -R $DATAGUY_USER:users $SAMBA_RO_PATH
chmod -R ug+rwx,o+rx-w $SAMBA_RW_PATH
chmod -R u+rwx,go+rx-w $SAMBA_RO_PATH

printInfo "Setting up groups for $DATAGUY_USER and $ADMINUSER to group users:"
usermod -a -G users dataguy
usermod -a -G users admin

printInfo "Setting up passwords for $DATAGUY_USER and $ADMINUSER:"
smbpasswd -a $DATAGUY_USER
smbpasswd -a $ADMINUSER

printInfo "Restarting samba service..."
service smbd restart
service smbd status

printInfo "Listing existing samba users:"
pdbedit -w -L

printWarn "Test samba shares with:"
printWarn "smbclient -U $DATAGUY_USER //localhost/$DATAGUY_USER"

printOK "Samba Configuration done."