#!/bin/bash

source ./util.sh
source ./config.sh


printInfo "Apache-MySQL and PhpMyAdmin setup started..."

apt-get install mysql-server mysql-client
apt-get install php5-mysql
apt-get install phpmyadmin

dpkg-reconfigure mysql-server-5.5
dpkg-reconfigure phpmyadmin

a2disconf phpmyadmin

printInfo "Restarting apache..."
service apache2 restart

printInfo "Checking apache2 configuration"
apache2ctl configtest

printOK "Mysql and phpmyadmin setup done!"
printWarn "Check http://www.$DOMAIN/phpmyadmin"