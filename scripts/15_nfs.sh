#!/bin/bash

source ./util.sh
source ./config.sh

printInfo "NFS setup started..."

printInfo "Creating $DATAGUY_USER user..."
adduser $DATAGUY_USER

printInfo "Installing packages..."
apt-get install nfs-common nfs-kernel-server

printInfo "Restarting nfs-kernel-server service..."
service nfs-kernel-server restart


printInfo "Creating directories for NFS shares..."
mkdir -vp $NFS_RW_PATH
mkdir -vp $NFS_RO_PATH

printInfo "Setting up permissions and ownership on $NFS_RW_PATH, $NFS_RO_PATH for user $DATAGUY_USER"
chown -R $DATAGUY_USER:users $NFS_RW_PATH
chown -R $DATAGUY_USER:users $NFS_RO_PATH
chmod -R ug+rwx,o+rx-w $NFS_RW_PATH
chmod -R u+rwx,go+rx-w $NFS_RO_PATH


printInfo "Changing contents of etc/exports..."
var="$NFS_PATH"
find $NEWROOT/etc/exports -type f -exec sed -i "s|\/srv\/data|$var|g" {} \;
var="$NFS_RW_PATH"
find $NEWROOT/etc/exports -type f -exec sed -i "s|\/srv\/data\/rw|$var|g" {} \;
var="$NFS_RO_PATH"
find $NEWROOT/etc/exports -type f -exec sed -i "s|\/srv\/data\/ro|$var|g" {} \;
var="$IPRANGE"
find $NEWROOT/etc/exports -type f -exec sed -i "s|10\.10\.10\.0\/24|$var|g" {} \;
var="$HOST_IP_RANGE"
find $NEWROOT/etc/exports -type f -exec sed -i "s|10\.10\.0\.0\/8|$var|g" {} \;

printInfo "Copying $NEWROOT/etc/exports in /etc/exports..."
cp -avr $NEWROOT/etc/exports /etc/exports

printInfo "Restarting nfs-kernel-server service..."
service nfs-kernel-server restart

printInfo "Making shares public..."
exportfs -a


printInfo "Creating nfs folders for remote access..."
mkdir -vp $NFS_RW_REMOTE
mkdir -vp $NFS_RO_REMOTE

printInfo "Changing ownership and group of the chreated folders:"
chmod $DATAGUY_USER:users $NFS_RW_REMOTE
chmod $DATAGUY_USER:users $NFS_RO_REMOTE


printInfo "Checking $DOMAIN..."
rpcinfo -p $DOMAIN

printInfo "Mounting nfs shares..."
mount -vvv $MY_NFS_RO_DIR -oro,vers=4,proto=tcp,port=$MY_NFS_PORT,sec=sys $NFS_RO_REMOTE
mount -vvv $MY_NFS_RW_DIR -orw,vers=4,proto=tcp,port=$MY_NFS_PORT,sec=sys $NFS_RW_REMOTE

printOK "NFS Setup completed!"
printWarn "Modify /etc/fstab manually for mounting shares on system startup!"
printWarn "Check $DATAGUY_USER id and change it if necessary to write on shares!"