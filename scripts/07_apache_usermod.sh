#!/bin/bash

source ./util.sh
source ./config.sh


printInfo "Apache-Usermod setup started..."

mkdir /home/$ADMINUSER/public_html
cp -avr $NEWROOT/home/user/public_html/* /home/$ADMINUSER/public_html/

printInfo "Changing /home/$ADMINUSER permissions..."
chmod 701 /home/$ADMINUSER
chmod 705 /home/$ADMINUSER/public_html
chown -R $ADMINUSER:$ADMINUSER /home/$ADMINUSER/public_html

printInfo "Checking apache2 configuration"
apache2ctl configtest

printOK "Usermod setup done!"
printWarn "Check http://www.$DOMAIN/~$ADMINUSER/"