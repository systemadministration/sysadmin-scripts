#!/bin/bash

source ./util.sh
source ./config.sh

################################################################################
# STATIC IP CONFIG
################################################################################
printInfo "STATIC IP CONFIG..."

INTERFACES_FILE=$NEWROOT/etc/network/interfaces

ifdown eth0

var="address $LOCALIP"
sed -i "s|address\s172\.31\.129\.176|$var|g" "$INTERFACES_FILE"

var="netmask $NETMASK"
sed -i "s|netmask\s255\.255\.248\.0|$var|g" "$INTERFACES_FILE"

var="gateway $GATEWAY"
sed -i "s|gateway\s172\.31\.128\.1|$var|g" "$INTERFACES_FILE"


# replace $INTERFACES_FILE
cp -avr $INTERFACES_FILE /etc/network/interfaces


################################################################################
# NAMESERVER CONFIG
################################################################################

echo "nameserver 8.8.8.8" > /etc/resolv.conf


################################################################################
# SSH CONFIG
################################################################################
printInfo "SSH CONFIG..."

mkdir /home/$ADMINUSER/.ssh
cp -avr $NEWROOT/home/user/.ssh/* /home/$ADMINUSER/.ssh/
chown -R $ADMINUSER:$ADMINUSER /home/$ADMINUSER/.ssh

# replace ssh config file
cp -avr $NEWROOT/etc/ssh/sshd_config /etc/ssh/sshd_config

printInfo "ssh service restarting..."
service ssh restart

printInfo "Enabling ssh on system startup..."
update-rc.d ssh enable



################################################################################
# FIREWALL CONFIG
################################################################################
printInfo "FIREWALL CONFIG"

bash $SHARE_LOC/scripts/disable_firewall.sh

printInfo "Replacing firewall rules..."

cp -avr $NEWROOT/etc/firewall.conf /etc/firewall.conf

# RESTORE IP TABLES FROM FILE
iptables-restore < /etc/firewall.conf

# RESTORE IP TABLES IN STARTUP
cp -avr $NEWROOT/etc/network/if-up.d/iptables /etc/network/if-up.d/iptables


printOK "NETWORK CONFIG DONE"
printWarn "TURN OFF VM AND MAKE A BRIDGED CONNECTION!!!!"