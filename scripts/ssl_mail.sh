#!/bin/bash

source ./util.sh
source ./config.sh

mkdir $MAIL_SSL_KEY_LOC

printInfo "Creating dovecot certificates:"

openssl req -new -x509 -days 3650 -nodes -out $MAIL_SSL_PEM_LOC/$MAILSSLDOVECOT.pem -keyout $MAIL_SSL_KEY_LOC/$MAILSSLDOVECOT.key -subj "/C=$SSL_COUNTRY/ST=$SSL_STATE/L=$SSL_LOCALITY/O=$SSL_ORGANIZATION_NAME/OU=$MAIL_ORGANIZATION_UNIT/CN=$MAIL_COMMON_NAME"
openssl req -new -x509 -days 3650 -nodes -out $MAIL_SSL_PEM_LOC/$MAILSSLPOSTFIX.pem -keyout $MAIL_SSL_KEY_LOC/$MAILSSLPOSTFIX.key -subj "/C=$SSL_COUNTRY/ST=$SSL_STATE/L=$SSL_LOCALITY/O=$SSL_ORGANIZATION_NAME/OU=$MAIL_ORGANIZATION_UNIT/CN=$MAIL_COMMON_NAME"

printInfo "Setting certificate owners and permissions..."
chown root:ssl-cert $MAIL_SSL_KEY_LOC/$MAILSSLPOSTFIX.key
chown root:ssl-cert $MAIL_SSL_KEY_LOC/$MAILSSLDOVECOT.key
chmod u=r,g=r,o= $MAIL_SSL_KEY_LOC/$MAILSSLPOSTFIX.key
chmod u=r,g=r,o= $MAIL_SSL_KEY_LOC/$MAILSSLDOVECOT.key

chown root:root $$MAIL_SSL_PEM_LOC/$MAILSSLPOSTFIX.pem
chown root:root $MAIL_SSL_PEM_LOC/$MAILSSLDOVECOT.pem
chmod u=rw,g=r,o=r $MAIL_SSL_PEM_LOC/$MAILSSLPOSTFIX.pem
chmod u=rw,g=r,o=r $MAIL_SSL_PEM_LOC/$MAILSSLDOVECOT.pem
printOK "Certificates restored!"

printOK "dovecot certificates:"
ls -lah $MAIL_SSL_PEM_LOC/$MAILSSLDOVECOT.pem $MAIL_SSL_KEY_LOC/$MAILSSLDOVECOT.key

printOK "postfix certificates:"
ls -lah $MAIL_SSL_PEM_LOC/$MAILSSLPOSTFIX.pem $MAIL_SSL_KEY_LOC/$MAILSSLPOSTFIX.key