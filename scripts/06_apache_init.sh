#!/bin/bash

source ./util.sh
source ./config.sh


printInfo "Apache setup started..."

printInfo "Installing packages..."

apt-get install apache2
apt-get install lynx
apt-get install php5
apt-get install ldap-utils
apt-get install libapache2-mod-authnz-external pwauth
apt-get install libapache2-mod-php5

printInfo "Starting apache service..."
service apache2 restart

printInfo "Disabling all virtual hosts..."
cd /etc/apache2/sites-available
a2dissite *

printInfo "Disabling necessary mods..."
a2dismod userdir
a2dismod status
a2dismod rewrite
a2dismod ssl
a2dismod authnz_ldap
a2disconf phpmyadmin

printInfo "Restarting apache service..."
service apache2 restart


printInfo "Replacing mod configurations..."
cp $NEWROOT/etc/apache2/mods-available/status.conf /etc/apache2/mods-available/status.conf
printOK "replaced: /etc/apache2/mods-available/status.conf"
cp $NEWROOT/etc/apache2/mods-available/ssl.conf /etc/apache2/mods-available/ssl.conf
printOK "replaced: /etc/apache2/mods-available/ssl.conf"


printInfo "Working on: var/www/ and var/web/"

printInfo "Replacing domain names..."
var="$DOMAIN"
find $NEWROOT/var/www/ -type f -exec sed -i "s|mehmet91\.est|$var|g" {} \;

printInfo "Replacing and renaming vhost aliases..."
var="$SITE_ALIAS1"
find $NEWROOT/var/www/ -type f -exec sed -i "s|alias1|$var|g" {} \;
var="$SITE_ALIAS2"
find $NEWROOT/var/www/ -type f -exec sed -i "s|alias2|$var|g" {} \;
mv -v $NEWROOT/var/www/vhosts/alias1 $NEWROOT/var/www/vhosts/$SITE_ALIAS1
mv -v $NEWROOT/var/www/vhosts/alias2 $NEWROOT/var/www/vhosts/$SITE_ALIAS2

cp -avr $NEWROOT/var/www /var/
cp -avr $NEWROOT/var/web /var/

printOK "var/www/ and var/web/"


printInfo "Working on: etc/apache2/sites-available"

printInfo "Replacing domain names..."
var="$DOMAIN"
find $NEWROOT/etc/apache2/sites-available/ -type f -exec sed -i "s|mehmet91\.est|$var|g" {} \;

printInfo "Replacing allowed ips..."
var="#Allow from 10.73"
find $NEWROOT/etc/apache2/sites-available/ -type f -exec sed -i "s|Allow\sfrom\s10\.73|$var|g" {} \;
var="Allow from $IPRANGE"
find $NEWROOT/etc/apache2/sites-available/ -type f -exec sed -i "s|Allow\sfrom\s10\.10\.10\.0\/24|$var|g" {} \;

printInfo "Replacing vhost alias paths..."
var="$SITE_ALIAS1"
find $NEWROOT/etc/apache2/sites-available/ -type f -exec sed -i "s|alias1|$var|g" {} \;
var="$SITE_ALIAS2"
find $NEWROOT/etc/apache2/sites-available/ -type f -exec sed -i "s|alias2|$var|g" {} \;


printInfo "Turning off RewriteEngine..."
var="RewriteEngine off"
find $NEWROOT/etc/apache2/sites-available/ -type f -exec sed -i "s|RewriteEngine\son|$var|g" {} \;

printInfo "Backing up /etc/apache2/sites-available"
mkdir /etc/apache2/sites-available/_bak
mv -v /etc/apache2/sites-available/* /etc/apache2/sites-available/_bak/

printInfo "Copying sites-available into /etc/apache2/sites-available/"
cp -avr $NEWROOT/etc/apache2/sites-available/0-www.conf /etc/apache2/sites-available/
cp -avr $NEWROOT/etc/apache2/sites-available/1-webmail.conf /etc/apache2/sites-available/
cp -avr $NEWROOT/etc/apache2/sites-available/2-sample1.conf /etc/apache2/sites-available/
cp -avr $NEWROOT/etc/apache2/sites-available/3-sample2.conf /etc/apache2/sites-available/
cp -avr $NEWROOT/etc/apache2/sites-available/5-alias1.conf /etc/apache2/sites-available/
cp -avr $NEWROOT/etc/apache2/sites-available/6-alias2.conf /etc/apache2/sites-available/

printInfo "Enabling required mods..."
a2enmod rewrite
a2enmod authnz_ldap
a2enmod authnz_external

printInfo "Enabling sites..."
cd /etc/apache2/sites-available
a2ensite *

printInfo "Enabling apache in system startup..."
update-rc.d apache2 enable

printInfo "Restarting apache..."
service apache2 restart

printInfo "Checking apache2 configuration"
apache2ctl configtest

printOK "Apache initial setup completed"
printWarn "Run ./apache/conf_enabled.sh"
printWarn "Run ./apache/mods_enabled.sh"
printWarn "Run other module scripts!"
#printWarn "FIRST RUN: ./ssl_apache.sh, THEN RUN: ./12_apache_ssl.sh"