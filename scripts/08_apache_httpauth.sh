#!/bin/bash

source ./util.sh
source ./config.sh


printInfo "Apache-HTTP Auth setup started..."

mkdir /etc/htpasswd

printInfo "Create new user with name:$ADMINUSER for .htpasswd:"
htpasswd -c /etc/htpasswd/.htpasswd $ADMINUSER

printInfo "Checking apache2 configuration"
apache2ctl configtest

printOK "HTTP Auth setup done!"
printWarn "Check http://sample1.$DOMAIN/sample1/sample1.html"