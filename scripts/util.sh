#!/bin/bash

function printInfo {
	echo -e "\033[0;96m[ INFO ] "$1"\033[0m"
}

function printOK {
    echo -e "\033[0;32m[ OK ] "$1"\033[0m"
}

function printWarn {
    echo -e "\033[0;33m[ WARNING ] "$1"\033[0m"
}

function printFailed {
    echo -e "\033[0;31m[ FAILED ] "$1"\033[0m"
}
