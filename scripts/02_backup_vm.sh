#!/bin/bash

source ./util.sh
source ./config.sh

printInfo "Backup VM started..."

cp -avr /etc /etc_bak

cp -avr /var/log/bind9 /var/log/bind9_bak
cp -avr /var/log/mail.log /var/log/mail.log.bak
cp -avr /var/log/messages /var/log/messages.bak
cp -avr /var/log/dovecot /var/log/dovecot_bak
cp -avr /var/www /var/log/www_bak

cp -avr /home /home_bak

cp -avr /opt/roundcube /opt/roundcube_bak

printOK "Backup VM done!"