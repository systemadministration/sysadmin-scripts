#!/bin/bash

source ./util.sh
source ./config.sh



printInfo "Mail setup started"

printInfo "Creating mailuser"
adduser mailuser


apt-get install ntp

printInfo "replacing /etc/ntp.conf"
cp -avr $NEWROOT/etc/ntp.conf /etc/ntp.conf
service ntp restart
service ntp status


printInfo "Installing packages"
apt-get install postfix dovecot-imapd spamassassin alpine procmail

printInfo "Starting services..."
service postfix restart
service dovecot restart
service spamassassin restart


printInfo "Configuring postfix..."

dpkg-reconfigure postfix
# don't forget to write hostname.domain

POSTFIXMAINFILE=$NEWROOT/etc/postfix/main.cf
var="$HOSTNAME.$DOMAIN"
sed -i "s|host59\.mehmet91\.est|$var|g" "$POSTFIXMAINFILE"
cp -avr $POSTFIXMAINFILE /etc/postfix/main.cf
printOK "/etc/postfix/main.cf"

POSTFIXMASTERFILE=$NEWROOT/etc/postfix/master.cf
cp -avr $POSTFIXMASTERFILE /etc/postfix/master.cf
printOK "/etc/postfix/master.cf"

POSTFIXCANONICALFILE=$NEWROOT/etc/postfix/canonical
var="$HOSTNAME.$DOMAIN"
sed -i "s|host59\.mehmet91\.est|$var|g" "$POSTFIXCANONICALFILE"
var="$DOMAIN"
sed -i "s|mehmet91\.est|$var|g" "$POSTFIXCANONICALFILE"
cp -avr $POSTFIXCANONICALFILE /etc/postfix/canonical

printInfo "compiling /etc/postfix/canonical"
postmap /etc/postfix/canonical

printWarn "Check /etc/postfix/canonical.db"

MAILNAMEFILE=$NEWROOT/etc/mailname
var="$DOMAIN"
sed -i "s|mehmet91\.est|$var|g" "$MAILNAMEFILE"
cp -avr $MAILNAMEFILE /etc/mailname
printOK "/etc/mailname"


ALIASESFILE=$NEWROOT/etc/aliases
cp -avr $ALIASESFILE /etc/aliases
newaliases

printOK "/etc/aliases"

postfix reload
service postfix restart

update-rc.d postfix enable
printOK "Postfix starts at system startup"

printOK "Postfix done!"




printInfo "Configuring alpine..."

ALPINECONF=$NEWROOT/etc/pine.conf
var="$DOMAIN"
sed -i "s|mehmet91\.est|$var|g" "$ALPINECONF"
cp -avr $ALPINECONF /etc/pine.conf

printOK "/etc/pine.conf"
printOK "Alpine done!"




printInfo "Configuring dovecot..."

printWarn "Skipping logging configuration"

printInfo "Replacing dovecot main configuration files..."
cp -avr $NEWROOT/etc/dovecot/conf.d/10-master.conf /etc/dovecot/conf.d/10-master.conf
cp -avr $NEWROOT/etc/dovecot/conf.d/10-ssl.conf /etc/dovecot/conf.d/10-ssl.conf
cp -avr $NEWROOT/etc/dovecot/conf.d/10-auth.conf /etc/dovecot/conf.d/10-auth.conf
cp -avr $NEWROOT/etc/dovecot/conf.d/10-mail.conf /etc/dovecot/conf.d/10-mail.conf
cp -avr $NEWROOT/etc/dovecot/conf.d/10-logging.conf /etc/dovecot/conf.d/10-logging.conf

printInfo "Setting /var/mail permissions"
chmod 2775 /var/mail
chown root:mail /var/mail
chgrp -R mail /var/mail


update-rc.d dovecot enable
printOK "Dovecot starts at system startup"

printOK "Dovecot done!"



printInfo "Configuring Spamassassin"
cp -avr $NEWROOT/home/mailuser/.procmailrc /home/mailuser/.procmailrc

update-rc.d spamassassin enable
printOK "Spamassassin starts at system startup"

printInfo "Restarting services..."
service postfix restart
service dovecot restart
service spamassassin restart
printOK "ALL DONE!!!"

printWarn "Make sure mailuser exists"
printWarn "Run postconf -n"
printWarn "Run doveconf -n"
printWarn "Set certificates manually!!!"
printWarn "Run: ./ssl_mail.sh then fix these files:"
printWarn "/etc/postfix/main.cf"
printWarn "/etc/dovecot/conf.d/10-ssl.conf"