#!/bin/bash

source ./util.sh
source ./config.sh

printInfo "LDAP setup started..."

printInfo "Installing packages..."
apt-get install ldap-utils


printWarn "Make sure ldap certificate is present!"

printInfo "Changing ownership and permissions of the certificate..."
chown root:root $LDAP_CERT_PATH
chmod u=rw,g=r,o=r $LDAP_CERT_PATH


printInfo "Changing contents of $NEWROOT/etc/ldap/ldap.conf..."
var="$LDAP_CERT_PATH"
find $NEWROOT/etc/ldap/ldap.conf -type f -exec sed -i "s|\/etc\/ssl\/certs\/ldapCA\.pem|$var|g" {} \;
var="$LDAP_URI"
find $NEWROOT/etc/ldap/ldap.conf -type f -exec sed -i "s|ldaps:\/\/ldap\.teacher\.est\/|$var|g" {} \;
var="$LDAP_BASE"
find $NEWROOT/etc/ldap/ldap.conf -type f -exec sed -i "s|dc\=sandbox\,dc\=est|$var|g" {} \;


printOK "LDAP config done!"
printWarn "Test it:"
printWarn "ldapsearch -x uid=mehmet91"
printWarn "ldapsearch -WD 'cn=Mehmet Can Boysan,ou=Students,$LDAP_BASE'"