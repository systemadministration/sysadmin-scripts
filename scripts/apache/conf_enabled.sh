#!/bin/bash

a2enconf charset
a2enconf javascript-common
a2enconf localized-error-pages
a2enconf other-vhosts-access-log
a2enconf owncloud
a2enconf security
a2enconf serve-cgi-bin
