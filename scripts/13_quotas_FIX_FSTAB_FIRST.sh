#!/bin/bash

source ./util.sh
source ./config.sh


printInfo "Quota configuration started..."

printInfo "Installing packages..."
apt-get install quota

printInfo "Adding $DATAGUY_USER user..."
adduser $DATAGUY_USER

printInfo "Quota check..."
quotacheck -cum /

printInfo "Turning on quotas..."
quotaon /

printInfo "Setting $DATAGUY_USER quota to 10MB..."
edquota $DATAGUY_USER

printInfo "Check quotas of all users..."
repquota -a
