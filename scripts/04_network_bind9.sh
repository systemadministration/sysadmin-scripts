#!/bin/bash

source ./util.sh
source ./config.sh


################################################################################
# Change Sources
################################################################################
printInfo "Configuring etc/apt/sources.list..."
cp -avr $NEWROOT/etc/apt/sources.list /etc/apt/sources.list

printInfo "Updating packages..."
apt-get update

################################################################################
# DNS RESOLVER
################################################################################

printInfo "Installing bind9..."

apt-get install bind9 bind9utils bind9-doc dnsutils


printInfo "Changing FQDN hostname..."

HOSTNAME_FILE=$NEWROOT/etc/hostname
echo "$HOSTNAME.$DOMAIN" > $HOSTNAME_FILE
cp -avr $HOSTNAME_FILE /etc/hostname


printInfo "Replacing hosts file..."

cp -avr $NEWROOT/etc/hosts /etc/hosts


################################################################################
# BIND9 CONFIG
################################################################################
printInfo "BIND9 CONFIG"

#echo "Replacing logs..."
#BINDLOGS=$NEWROOT/var/log/bind9
#cp -avr $BINDLOGS/* /var/log/bind9/

#echo "Setting permissions and ownerships of log files for bind9..."
#chown -R bind:bind /var/log/bind9
#chmod -R 640 /var/log/bind9


printWarn "Skipping logging configuration"


printInfo "Setting configuration files..."

OPTIONS_FILE=$NEWROOT/etc/bind/named.conf.options

#goodclients
var="$IPRANGE"
sed -i "s|172\.31\.129\.0\/24|$var|g" "$OPTIONS_FILE"

var="$HOST_IP_RANGE"
sed -i "s|172\.31\.128\.0\/24|$var|g" "$OPTIONS_FILE"

var="$LOCALIP"
sed -i "s|172\.31\.129\.176|$var|g" "$OPTIONS_FILE"


LOCAL_FILE=$NEWROOT/etc/bind/named.conf.local

var="$DOMAIN.zone"
sed -i "s|mehmet91\.est\.zone|$var|g" "$LOCAL_FILE"

var="$DOMAIN"
sed -i "s|mehmet91\.est|$var|g" "$LOCAL_FILE"

var="$ARPAZONE"
sed -i "s|10\.10\.10\.in-addr\.arpa\.zone|$var|g" "$LOCAL_FILE"

var="$INADDRARPA"
sed -i "s|129\.31\.172\.in-addr\.arpa|$var|g" "$LOCAL_FILE"

var="$GATEWAY"
sed -i "s|172\.31\.128\.1|$var|g" "$LOCAL_FILE"

var="$TOPDMN"
sed -i "s|\"est\"|\"$var\"|g" "$LOCAL_FILE"



printInfo "Setting zone files..."

ARPAZONEFILE=$NEWROOT/etc/bind/zones/129.31.172.in-addr.arpa.zone

var="$INADDRARPA"
sed -i "s|129\.31\.172\.in-addr\.arpa|$var|g" "$ARPAZONEFILE"

var="$DOMAIN"
sed -i "s|mehmet91\.est|$var|g" "$ARPAZONEFILE"

var="$IPEND"
sed -i "s|176|$var|g" "$ARPAZONEFILE"

var="2017052211"
sed -i "s|2017051211|$var|g" "$ARPAZONEFILE"

MAINZONEFILE=$NEWROOT/etc/bind/zones/mehmet91.est.zone

var="$DOMAIN"
sed -i "s|mehmet91\.est|$var|g" "$MAINZONEFILE"

var="$LOCALIP"
sed -i "s|172\.31\.129\.176|$var|g" "$MAINZONEFILE"

var="$HOSTNAME"
sed -i "s|host59|$var|g" "$MAINZONEFILE"

var="2017052218"
sed -i "s|2017051118|$var|g" "$MAINZONEFILE"

echo "$SITE_ALIAS1 IN  CNAME   $HOSTNAME" >> $MAINZONEFILE
echo "$SITE_ALIAS2 IN  CNAME   $HOSTNAME" >> $MAINZONEFILE


printInfo "Renaming zone files..."
mv $ARPAZONEFILE $NEWROOT/etc/bind/zones/$ARPAZONE
mv $MAINZONEFILE $NEWROOT/etc/bind/zones/$DOMAIN.zone

printInfo "Copying bind folder to VM..."
cp -avr $NEWROOT/etc/bind/named.conf /etc/bind/
cp -avr $NEWROOT/etc/bind/named.conf.local /etc/bind/
cp -avr $NEWROOT/etc/bind/named.conf.default-zones /etc/bind/
cp -avr $NEWROOT/etc/bind/named.conf.local /etc/bind/
cp -avr $NEWROOT/etc/bind/named.conf.logging /etc/bind/
cp -avr $NEWROOT/etc/bind/named.conf.options /etc/bind/

cp -avr $NEWROOT/etc/bind/zones /etc/bind/

printInfo "Setting permissions..."
chown bind:bind /etc/bind/named.conf*
chown bind:bind /etc/bind/named.conf.logging
chown bind:bind /etc/bind/rndc.key
chown -R bind:bind /etc/bind/zones


################################################################################
# NAMESERVER CONFIG
################################################################################
printInfo "NAMESERVER CONFIG..."

echo "nameserver 127.0.0.1" > /etc/resolv.conf
echo "domain $DOMAIN" >> /etc/resolv.conf
echo "search $DOMAIN" >> /etc/resolv.conf


################################################################################
# FINALIZE
################################################################################
printInfo "Restarting bind9..."
service bind9 restart

printInfo "bind9 status..."
service bind9 status

printInfo "Setting bind9 to start at system start..."
update-rc.d bind9 enable

printOK "bind9 config done!"
printWarn "Suggesting rebooting the VM!"