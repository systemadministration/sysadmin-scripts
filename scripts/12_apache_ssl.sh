#!/bin/bash

source ./util.sh
source ./config.sh


printInfo "Apache-SSL setup started..."


printInfo "Enabling RewriteEngine in virtual hosts..."
var="RewriteEngine on"
find /etc/apache2/sites-available/ -type f -exec sed -i "s|RewriteEngine\soff|$var|g" {} \;

printInfo "Changing ssl certificate locations in virtual-hosts..."
var="$APACHE_CERT_LOC/$APACHEROOTCA"
find $NEWROOT/etc/apache2/sites-available/ -type f -exec sed -i "s|\/etc\/ssl\/cacert\/rootCA|$var|g" {} \;

var="$APACHE_CERT_LOC/www-$APACHECERTSUFFIX"
find $NEWROOT/etc/apache2/sites-available/ -type f -exec sed -i "s|\/etc\/ssl\/cacert\/www|$var|g" {} \;
var="$APACHE_CERT_LOC/webmail-$APACHECERTSUFFIX"
find $NEWROOT/etc/apache2/sites-available/ -type f -exec sed -i "s|\/etc\/ssl\/cacert\/webmail|$var|g" {} \;
var="$APACHE_CERT_LOC/sample1-$APACHECERTSUFFIX"
find $NEWROOT/etc/apache2/sites-available/ -type f -exec sed -i "s|\/etc\/ssl\/cacert\/sample1|$var|g" {} \;
var="$APACHE_CERT_LOC/sample2-$APACHECERTSUFFIX"
find $NEWROOT/etc/apache2/sites-available/ -type f -exec sed -i "s|\/etc\/ssl\/cacert\/sample2|$var|g" {} \;
var="$APACHE_CERT_LOC/$SITE_ALIAS1-$APACHECERTSUFFIX"
find $NEWROOT/etc/apache2/sites-available/ -type f -exec sed -i "s|\/etc\/ssl\/cacert\/$SITE_ALIAS1|$var|g" {} \;
var="$APACHE_CERT_LOC/$SITE_ALIAS2-$APACHECERTSUFFIX"
find $NEWROOT/etc/apache2/sites-available/ -type f -exec sed -i "s|\/etc\/ssl\/cacert\/$SITE_ALIAS2|$var|g" {} \;


printInfo "Copying sites-available into /etc/apache2/sites-available/"
cp -avr $NEWROOT/etc/apache2/sites-available/0-www-ssl.conf /etc/apache2/sites-available/
cp -avr $NEWROOT/etc/apache2/sites-available/1-webmail-ssl.conf /etc/apache2/sites-available/
cp -avr $NEWROOT/etc/apache2/sites-available/2-sample1-ssl.conf /etc/apache2/sites-available/
cp -avr $NEWROOT/etc/apache2/sites-available/3-sample2-ssl.conf /etc/apache2/sites-available/
cp -avr $NEWROOT/etc/apache2/sites-available/5-alias1-ssl.conf /etc/apache2/sites-available/
cp -avr $NEWROOT/etc/apache2/sites-available/6-alias2-ssl.conf /etc/apache2/sites-available/


printInfo "Enabling required mods..."
a2enmod ssl
a2enmod authnz_ldap


printInfo "Enabling sites..."
cd /etc/apache2/sites-available
a2ensite *


printInfo "Restarting apache service"
service apache2 restart


printInfo "Checking apache2 configuration"
apache2ctl configtest

printOK "Apache ssl config done!"
printWarn "Run ./apache/conf_enabled.sh"
printWarn "Run ./apache/mods_enabled.sh"
printWarn "Run ./ssl_apache.sh"