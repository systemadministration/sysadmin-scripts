#!/bin/bash

source ./util.sh
source ./config.sh

function create_rootca() {

    printInfo "Creating dir $APACHE_CERT_LOC"
    mkdir -vp $APACHE_CERT_LOC
    cd $APACHE_CERT_LOC

    printInfo "Creating RootCA..."
    # Create the private root key for CA and add a password to protect it.
    openssl genrsa -des3 -out $APACHEROOTCA.key 4096

    # Sign this CA certificate.
    openssl req -x509 -new -nodes -key $APACHEROOTCA.key -sha256 -days 1024 -out $APACHEROOTCA.pem -subj "/emailAddress=$AP_EMAIL/C=$SSL_COUNTRY/ST=$SSL_STATE/L=$SSL_LOCALITY/O=$SSL_ORGANIZATION_NAME/OU=$AP_ORGANIZATION_UNIT/CN=$AP_COMMON_NAME"
    # Country name=EE
    # State=Tartumaa
    # Locality=Tartu
    # Organization=SA Lab
    # Organizational Unit=Web Masters
    # Common Name=CA of SA Lab
    # emailAddress=mailuser@mehmet91.est

    printOK "RootCA $APACHE_CERT_LOC/$APACHEROOTCA is created!"
}

function create_vhost_cert() {

    printInfo "Creating dir $APACHE_CERT_LOC"
    mkdir -vp $APACHE_CERT_LOC
    cd $APACHE_CERT_LOC

    printInfo "Creating certificate for <<$1.$DOMAIN>>"
    # Create a (site)key for www.<your-domain>.est
    openssl genrsa -out $1-$APACHECERTSUFFIX.key 2048
    # Sign it
    openssl req -new -key $1-$APACHECERTSUFFIX.key -out $1-$APACHECERTSUFFIX.csr -subj "/emailAddress=$AP_EMAIL/C=$SSL_COUNTRY/ST=$SSL_STATE/L=$SSL_LOCALITY/O=$SSL_ORGANIZATION_NAME/OU=$AP_ORGANIZATION_UNIT/CN=$1.$DOMAIN"
    # Country name=EE
    # State=Tartumaa
    # Locality=Tartu
    # Organization=SA Lab
    # Organizational Unit=Web Admins
    # Common Name=www.mehmet91.est
    # Email=mailuser@mehmet91.est

    # CA must sign the www.mehmet91.est certificate request.
    openssl x509 -req -in $1-$APACHECERTSUFFIX.csr -CA $APACHEROOTCA.pem -CAkey $APACHEROOTCA.key -CAcreateserial -out $1-$APACHECERTSUFFIX.crt -days 500 -sha256
    printOK "$APACHE_CERT_LOC/$1-$APACHECERTSUFFIX.key done!"
    printOK "$APACHE_CERT_LOC/$1-$APACHECERTSUFFIX.csr done!"
    printOK "$APACHE_CERT_LOC/$1-$APACHECERTSUFFIX.crt done!"
}

create_rootca
create_vhost_cert "www"
create_vhost_cert "webmail"
create_vhost_cert "sample1"
create_vhost_cert "sample2"
create_vhost_cert "$SITE_ALIAS1"
create_vhost_cert "$SITE_ALIAS2"