#!/bin/bash

source ./util.sh
source ./config.sh

printInfo "Restore started..."

rm -vr $NEWROOT
mkdir $NEWROOT
cp -avr $SHARE_LOC/$BACKUP_NAME $NEWROOT
cd $NEWROOT
tar -xzvf $BACKUP_NAME

printOK "Restore done!"