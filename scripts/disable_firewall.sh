#!/bin/bash

# backup iptables
echo "Saving IPTABLES"
iptables-save > /etc/firewall.conf.bak

echo "Disabling firewall"
iptables -F
iptables -X
iptables -t nat -F
iptables -t nat -X
iptables -t mangle -F
iptables -t mangle -X
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT