#!/bin/bash

source ./util.sh
source ./config.sh


printInfo "Apache-PAM setup started..."

apt-get install libapache2-mod-authnz-external pwauth

printInfo "Enabling PAM mod..."
a2enmod authnz_external

printInfo "Restarting apache..."
service apache2 restart

printInfo "Checking apache2 configuration"
apache2ctl configtest

printOK "PAM setup done!"
printWarn "Check http://sample2.$DOMAIN/sample2/sample2.html"